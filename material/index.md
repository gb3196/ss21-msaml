# Mathematic(al Statistic)s and Applications of Machine Learning [[PDF](index.pdf)]

* [Course site](https://gitlab.com/dirk-deckert-lmu/ss21-msaml)


## Table of contents

1. [Week of April 12: Lecture 01](#week-of-april-12:-lecture-01)
2. [Overview of MAML topics](#overview-of-maml-topics)
3. [Overview of MSAML topics](#overview-of-msaml-topics)


## Weekly course material 


### Week of April 12: Lecture 01

Organization:

* [Get-to-know session](SS21-MsAML__01-1__Get_to_know_session.pdf)

Lectures notes:

* [Introduction and overview](SS21-MsAML__01-2__Introduction_and_overview.md)
* [Supervised learning setting](SS21-MsAML__01-3__Supervised_learning_setting.md)
* (S) [Statistical framework](SS21-MsAML__01-S1__Statistical_framework.md)

Exercises and solutions:

|       | Exercise                                                                                                      | Discussion | Solution |
|-------|---------------------------------------------------------------------------------------------------------------|------------|----------|
| 01-0  | [Python warmup](SS21-MsAML__01-X0__Python_warmup.ipynb)                                                       | #5         | included |
| 01-1  | [Set up a Python development environment](SS21-MsAML__01-X1__Set_up_a_Python_development_environment.md)      | #6         | included |
| 01-2  | [First steps with Numpy](SS21_MsAML__01-X2__First_steps_with_numpy.ipynb)                                     | #7         | included |
| 01-3  | [Plotting and obtaining data](SS21_MsAML__01-X3__Plotting_and_obtaining_data.ipynb)                           | #8         | included |
| 01-M1 | [Histograms from Covid-19 time series data](SS21_MsAML__01-XM1__Histograms_from_Covid-19_time_series_data.md) | #9         |          |
| 01-S1 | [Probability warm up](SS21-MsAML__01-XS1__Probability_warm_up.md)                                             | #10        |          |


## Course material overviews 


### Overview of MAML topics 

* [Introduction and overview](SS21-MsAML__01-2__Introduction_and_overview.md) 
* [Supervised learning setting](SS21-MsAML__01-3__Supervised_learning_setting.md)


### Overview of MSAML topics 

* (S) [Statistical framework](SS21-MsAML__01-S1__Statistical_framework.md)
