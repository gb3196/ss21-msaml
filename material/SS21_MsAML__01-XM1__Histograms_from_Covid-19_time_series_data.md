# Exercise 01-XM1: Histograms from Covid-19 time series data [[PDF](SS21_MsAML__01-XM1__Histograms_from_Covid-19_time_series_data.pdf)]

Let us apply the knowledge gained during the first steps playing with the `numpy`,
`matplotlib`, and `pandas` packages and obtain the newest Covid-19 time series
data from:

[https://github.com/datasets/covid-19](https://github.com/datasets/covid-19)

and produce histograms plots for

1. the total active, recovered, and fatal cases 
1. as well as for the respective daily increments,

for instance,

* for Germany and Sweden or any other two countries you are interested in,

and compare the data when normalized to

* the total population and 
* total conducted tests.

Just to give you some ideas how such plots could look like -- but feel free to
create your own plot designs:

[Figure: Sweden time series](./01/Covid-19 Germany and Sweden.pdf)
