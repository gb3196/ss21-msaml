# # Hello world of data science example

# Import the `pandas` and `matplotlib` package

import pandas as pd
import matplotlib.pyplot as plt

# Obtain the Iris data set

url = "https://raw.githubusercontent.com/jbrownlee/Datasets/master/iris.csv"
names = ['sepal length [cm]', 'sepal width [cm]',
         'petal length [cm]', 'petal width [cm]', 'iris type']
df = pd.read_csv(url, names=names)

# Have a quick look at the data

print(df)

# Plot the data with features $(x_1, x_2)\in\mathbb R^2$ where:
# * $x_1$ denotes the sepal length in cm and
# * $x_2$ denotes the sepal width in cm.

# Scatter plot of the data

df_setosa = df[df['iris type'] == 'Iris-setosa']
ax = df_setosa.plot.scatter(
    x='sepal length [cm]', y='sepal width [cm]', c='red',
    label='Iris-setosa')

df_setosa = df[df['iris type'] == 'Iris-versicolor']
ax = df_setosa.plot.scatter(
    x='sepal length [cm]', y='sepal width [cm]', c='green',
    label='Iris-versicolor', ax=ax)

df_setosa = df[df['iris type'] == 'Iris-virginica']
ax = df_setosa.plot.scatter(
    x='sepal length [cm]', y='sepal width [cm]', c='blue',
    label='Iris-virginica', ax=ax)

# Within a Jupyter Notebook the plot would show already. Not so on the console
# where we have to open and show the plotted graph.  Internally `pandas` uses
# the package `matplotlib` for plotting and we use the same to open a view of
# the last plotted graph that can be closed by pressing `q` by:

plt.show()
