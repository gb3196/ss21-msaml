<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__01-2__Introduction_and_overview</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-01-1-pdf">MsAML-01-1 [<a href="SS21-MsAML__01-2__Introduction_and_overview.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#introduction-and-overview">Introduction and overview</a></li>
<li><a href="#what-is-*artificial-intelligence*-(ai)?">What is <em>artificial intelligence</em> (AI)?</a>
<ul>
<li><a href="#acting-humanly:-the-turing-test-approach">Acting humanly: The Turing test approach</a></li>
<li><a href="#thinking-humanly:-the-cognitive-modeling-approach">Thinking humanly: The cognitive modeling approach</a></li>
<li><a href="#thinking-rationally:-the-“laws-of-thought”-approach">Thinking rationally: The “laws of thought” approach</a></li>
<li><a href="#acting-rationally:-the-rational-agent-approach">Acting rationally: The rational agent approach</a></li>
<li><a href="#influences">Influences</a></li>
<li><a href="#a-breakdown-of-historical-periods">A breakdown of historical periods</a></li>
</ul></li>
<li><a href="#what-is-machine-learning-(ml)?">What is machine learning (ML)?</a>
<ul>
<li><a href="#unsupervised-learning">Unsupervised learning</a></li>
<li><a href="#supervised-learning">Supervised learning</a>
<ul>
<li><a href="#dataism">Dataism</a></li>
</ul></li>
</ul></li>
<li><a href="#our-goals">Our goals</a></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="introduction-and-overview">Introduction and overview</h2>
<p>Before diving into the topic let us settle for a common vocabulary and what we would like to achieve with our study.</p>
<p>We start with a very broad overview and discussion of artificial intelligence taken from <a href="https://www.pearson.com/store/p/artificial-intelligence-a-modern-approach/P100000667303/9780136042594">Artificial Intelligence: A Modern Approach. Stuart J. Russell, Peter Norvig, Prentice Hall, 2010</a> and afterwards put machine learning and the aims of this lecture in perspective.</p>
<h2 id="what-is-artificial-intelligence-ai">What is <em>artificial intelligence</em> (AI)?</h2>
<ul>
<li>For many thousands of years, we have been trying to understand how we think;</li>
<li>AI attempts to go a step further:
<ul>
<li>in not to understand but also to build <em>intelligent</em> entities.</li>
</ul></li>
<li>AI is one of the newest fields in science and engineering (started shortly after World War II);</li>
<li>Possible definitions:
<ul>
<li><p>Acting humanly:</p>
<blockquote>
<p><em>“The art of creating machines that perform functions that require intelligence when performed by people.”</em></p>
<p>– Kurzweil, 1990</p>
</blockquote>
<blockquote>
<p><em>“The study of how to make computers do things at which, at the moment, people are better.”</em></p>
<p>– Rich and Knight, 1991</p>
</blockquote></li>
<li><p>Thinking humanly:</p>
<blockquote>
<p><em>“The exciting new effort to make computers think […] machines with minds, in the full and literal sense.”</em></p>
<p>– Haugeland, 1985</p>
</blockquote>
<blockquote>
<p><em>“[The automation of] activities that we associate with human thinking, activities such as decision-making, problem solving, learning […]”</em></p>
<p>– Bellman, 1978</p>
</blockquote></li>
<li><p>Acting rationally:</p>
<blockquote>
<p><em>“Computational Intelligence is the study of the design of intelligent agents.”</em></p>
<p>– Poole et al., 1998</p>
</blockquote>
<blockquote>
<p><em>“AI […] is concerned with intelligent behavior in artifacts.”</em></p>
<p>– Nilsson, 1998</p>
</blockquote></li>
<li><p>Thinking rationally:</p>
<blockquote>
<p><em>“The study of mental faculties through the use of computational models.”</em></p>
<p>– Charniak and McDermott, 1985</p>
</blockquote>
<blockquote>
<p><em>“The study of the computations that make it possible to perceive, reason, and act.”</em></p>
<p>– Winston, 1992</p>
</blockquote></li>
</ul></li>
</ul>
<h3 id="acting-humanly-the-turing-test-approach">Acting humanly: The Turing test approach</h3>
<ul>
<li>In <a href="https://academic.oup.com/mind/article/LIX/236/433/986238">1950 Turning</a> devised a test to provide a satisfactory operational definition of intelligence;</li>
<li>A computer passes the test if a human interrogator, after posing some written questions, cannot tell whether the written responses come from a person or from a computer;</li>
<li>The computer needs to posses the following features:
<ul>
<li><strong>natural language processing:</strong> to communicate in, e.g., English</li>
<li><strong>knowledge representation:</strong> to store the information</li>
<li><strong>automated reasoning:</strong> to use the stored information, to answer question, and to draw conclusions</li>
<li><strong>machine learning:</strong> to adapt to new circumstances, and extrapolate and detect patterns</li>
</ul></li>
<li>The Turing test remains relevant even today but less from the engineering and more from the philosophical stance, i.e., in the following spirit:
<ul>
<li>The quest for “artificial flight” succeeded when the Wright brothers and others stopped imitating birds and started using wind tunnels and learning about aerodynamics.</li>
<li>Aeronautical engineering texts do not define the goal of their field as making “machines that fly so exactly like pigeons that they can fool even other pigeons.”</li>
</ul></li>
</ul>
<h3 id="thinking-humanly-the-cognitive-modeling-approach">Thinking humanly: The cognitive modeling approach</h3>
<ul>
<li>To tell whether a program “thinks like a human” we need to learn what humanely thinking is:
<ul>
<li>Observe our thoughts as they go by;</li>
<li>Observe persons during a task</li>
<li>Psychological / neuroscientific experiments</li>
</ul></li>
<li>For our endeavor, however, it will be good practice to keep the fields such as cognitive science, neuroscience, psychology and philosophy separated as long as it is possible.</li>
</ul>
<h3 id="thinking-rationally-the-laws-of-thought-approach">Thinking rationally: The “laws of thought” approach</h3>
<p>Use of logical reasoning and argumentation. For an intentionally naive but characterizing example consider:</p>
<ul>
<li><p><strong>deduction:</strong> A general rule applied to a particular case implies a trivial result.</p>
<table>
<thead>
<tr class="header">
<th>Input</th>
<th>Implication</th>
<th>Example</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><em>RULE</em></td>
<td></td>
<td>On a planet its sun rises every day.</td>
</tr>
<tr class="even">
<td><em>CASE</em></td>
<td></td>
<td>We are on a planet.</td>
</tr>
<tr class="odd">
<td></td>
<td><em>RESULT</em></td>
<td>The sun rose every day.</td>
</tr>
</tbody>
</table>
<p>The realm of mathematics.</p></li>
<li><p><strong>induction:</strong> From a trivial result in a particular case we <em>hope</em> to infer the general rule.</p>
<table>
<thead>
<tr class="header">
<th>Input</th>
<th>Implication</th>
<th>Example</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td></td>
<td><em>RULE</em></td>
<td>On a planet its sun rises every day.</td>
</tr>
<tr class="even">
<td><em>CASE</em></td>
<td></td>
<td>We are on a planet.</td>
</tr>
<tr class="odd">
<td><em>RESULT</em></td>
<td></td>
<td>The sun rose every day.</td>
</tr>
</tbody>
</table>
<p>Mostly the realm of natural sciences and there is no guarantee but room for interpretation and dispute.</p></li>
<li><p><strong>abduction:</strong> From a general rule and a trivial result we <em>hope</em> to infer the particular case.</p>
<table>
<thead>
<tr class="header">
<th>Input</th>
<th>Implication</th>
<th>Example</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><em>RULE</em></td>
<td></td>
<td>On a planet its sun rises every day.</td>
</tr>
<tr class="even">
<td></td>
<td><em>CASE</em></td>
<td>We are on a planet.</td>
</tr>
<tr class="odd">
<td><em>RESULT</em></td>
<td></td>
<td>The sun rose every day.</td>
</tr>
</tbody>
</table>
<p>Similar to induction and but rather seldom used.</p></li>
</ul>
<h3 id="acting-rationally-the-rational-agent-approach">Acting rationally: The rational agent approach</h3>
<ul>
<li>Create <em>agents</em> (e.g., computer programs) that operate autonomously, perceive their environment, persist over a prolonged time period, adapt to change, and create and pursue goals.</li>
<li>Making correct inferences (as in the “laws of thought” approach) is the extreme case of being a rational agent.</li>
<li>In many situations, however, correct inferences are not possible, e.g.:
<ul>
<li>insufficient understanding of the environment;</li>
<li>not enough input data to base a decision on.</li>
</ul></li>
<li>A rational agent is one that acts so as to achieve the <em>best</em> outcome or, when there is uncertainty, the best expected outcome.</li>
</ul>
<p>We will go down this road:</p>
<ul>
<li>The standard of rationality is mathematically well defined and completely general.</li>
<li>We may exploit this, spell out specific designs, and check how they perform in certain environments. For instance:
<ul>
<li>The “probably approximately correct (PAC)” framework</li>
</ul></li>
<li>Human behavior, on the other hand, is well adapted for one specific environment and is defined by, well, the sum total of all the things that humans do.</li>
</ul>
<h3 id="influences">Influences</h3>
<p>Though AI is rather young it is a very multi-disciplinary field:</p>
<ul>
<li>Philosophy:
<ul>
<li>Can formal rules be used to draw valid conclusions?</li>
<li>How does the mind arise from a physical brain?</li>
<li>Where does knowledge come from?</li>
<li>How does knowledge lead to action?</li>
</ul></li>
<li>Mathematics:
<ul>
<li>What are the formal rules to draw valid conclusions?</li>
<li>What can be computed?</li>
<li>How do we reason with uncertain information?</li>
</ul></li>
<li>Neuroscience:
<ul>
<li>How do brains process information?</li>
</ul></li>
<li>Psychology
<ul>
<li>How do humans and animals think and act?</li>
</ul></li>
<li>Computer engineering
<ul>
<li>How can we build an efficient computer – the artifact that we want to charge with <em>intelligence</em>?</li>
</ul></li>
<li>Control theory and cybernetics:
<ul>
<li>How can artifacts operate under their own control?</li>
</ul></li>
<li>Linguistics:
<ul>
<li>How does language relate to thought?</li>
</ul></li>
<li>Finance and economics:
<ul>
<li>How should we make decisions so as to maximize payoff?</li>
<li>How should we do this when others may not go along?</li>
<li>How should we do this when the payoff may be far in the future?</li>
</ul></li>
<li>[…]</li>
</ul>
<h3 id="a-breakdown-of-historical-periods">A breakdown of historical periods</h3>
<ul>
<li><strong>1943–1955:</strong> The gestation of artificial intelligence
<ul>
<li>Model of artificial neurons by <a href="https://link.springer.com/article/10.1007/BF02478259">Warren McCulloch and Walter Pitts in 1943</a>;</li>
<li>Turning gave lectures on AI as soon as 1947.</li>
</ul></li>
<li><strong>1956:</strong> The birth of artificial intelligence
<ul>
<li>John McCarthy convinced Marvin Minsky, Claude Shannon, and Nathaniel Rochester to help him bring together U.S. researchers interested in automata theory, neural nets, and the study of intelligence at a two-month workshop in Dartmouth.</li>
</ul></li>
<li><strong>1952–1969:</strong> Early enthusiasm, great expectations
<ul>
<li>First problem solvers, game players, theorem provers;</li>
<li>John McCarthy referred to this period as the “Look, Ma, no hands!” era;</li>
<li>Creation of LISP;</li>
<li>Perceptron by <a href="https://psycnet.apa.org/record/1959-09865-001">Frank Rosenblatt in 1958</a>;</li>
<li>Adalines (adaptive linear neuron) by Bernie Widrow and Marcian Hoff in 1960 :cite:<code>widrow_adapting_1960</code>;</li>
</ul></li>
<li><strong>1966–1973:</strong> A dose of reality
<ul>
<li>Try and error – combinatorial explosion;</li>
<li>Lack of computational resources.</li>
</ul></li>
<li><strong>1969–1979:</strong> Knowledge-based systems: The key to power?
<ul>
<li>Algorithms using of domain-specific knowledge instead of general-purpose solvers;</li>
<li>Expert systems for medical diagnosis;</li>
<li>Incorporation of uncertainty.</li>
</ul></li>
<li><strong>1980–present:</strong> AI becomes an industry
<ul>
<li>Optimization of logistics;</li>
<li>Sudden boom but only few projects lived up to their expectations;</li>
<li>AI winter.</li>
</ul></li>
<li><strong>1986–present:</strong> The return of neural networks
<ul>
<li>The <em>back-propagation</em> algorithm for training neural networks was reinvented in :cite:<code>rumelhart_learning_1986</code>;</li>
</ul></li>
<li><strong>1987–present:</strong> AI adopts the scientific method
<ul>
<li>Hidden Markov models;</li>
<li>Bayesian networks;</li>
</ul></li>
<li><strong>1995–present:</strong>
<ul>
<li><p>The Internet pushes the development of intelligent (?) agents, e.g.:</p>
<ul>
<li>chatbots</li>
<li>recommender systems</li>
<li>aggregates</li>
</ul></li>
<li><p>Access to computation resources at sufficient speed.</p></li>
<li><p>The <em>big data</em> age: Huge amount of labeled training data available, e.g.:</p>
<ul>
<li>Dictionaries</li>
<li>Word corpora on different topics</li>
<li>Wordnets</li>
<li>Wikipedia</li>
<li>Google</li>
</ul></li>
<li><p>Founders of AI disappointed with current state:</p>
<ul>
<li>AI should return to its roots of striving for, in Herbert Simon’s words, “machines that think, that learn and that create.”</li>
</ul></li>
<li><p>Now long state-of-the-art – some examples:</p>
<ul>
<li>Spam fighting: Most adaptation done my machine learning algorithms</li>
<li>Speech recognition: Siri,</li>
<li>Face recognition: Facebook, Apple Photos, Google Photos</li>
<li>Game playing: IBM’s deep blue chess player against world champion Garry Kasparov</li>
<li>Autonomous planning and scheduling: NASA’s mars rover</li>
<li>Robotic vehicles: Tesla’s self-driven car</li>
<li>Machine Translation: Google Translate</li>
</ul></li>
<li><p>And in addition some astonishing academic advances that, even though, industry readiness may be still open:</p>
<ul>
<li>2011 IBM Watson triumphs at <em>Jeopardy!</em></li>
<li>2012 Google’s X Lab object recognition of cats in YouTube videos</li>
<li>2014 DeepMind’s AI playing Atari Games</li>
<li>2014 Generative Adversial Networks that generate or adapt content, such as, photorealistic photos, videos, 3D models</li>
<li>2015 DeepMind’s AlphaGo beats Go champion</li>
<li>[…]</li>
</ul></li>
</ul></li>
</ul>
<h2 id="what-is-machine-learning-ml">What is machine learning (ML)?</h2>
<p>Having a first overview, we come to the direction of our study, i.e., machine learning. With this we, as well, might disappoint the founders – in view of Herbert Simon’s quote above. However, in our defense we may claim that in whichever direction AI might develop, machine learning will at least be an extremely important stepping stone if not even stay an integral part in the field:</p>
<ul>
<li><p>ML is a subfield of AI:</p>
<blockquote>
<p><em>“[Machine learning] gives computers the ability to learn without being explicitly programmed”</em></p>
<p>– Samuel, 1959</p>
</blockquote>
<p>Put differently, one seeks algorithms which have general strategies at their expense or are able to adapt their strategies to solve a to some extend previously unknown task.</p></li>
<li><p>ML will help us dealing with large amounts of data:</p>
<ul>
<li>data compression;</li>
<li>correlation detection;</li>
<li>pattern recognition;</li>
<li>structuring data;</li>
<li>classification of data;</li>
<li>extrapolation / prediction;</li>
<li>model fitting;</li>
<li>data driven decision support;</li>
<li>[…]</li>
</ul></li>
</ul>
<h3 id="unsupervised-learning">Unsupervised learning</h3>
<p>Structuring or filtering of data on the basis of predefined strategy and ontologies. Examples:</p>
<ul>
<li>Data compression and dimensionality reduction;</li>
<li>Assisted feature selection, correlation detection;</li>
<li>Identification of data clusters, such as, galaxy clusters in astronomic observational data, scattering events in a particle collider, etc.;</li>
<li>Process mining;</li>
<li>Language modeling, e.g., with wordnets;</li>
<li>Identification of customer groups based on the Facebook likes;</li>
<li>[…]</li>
</ul>
<p>As a fictitious example, consider the following plot of 2d data points with the colors indicating a relation between the data points. From these relations the shaded regions may be inferred by an unsupervised machine learning algorithm, e.g., by a nearest-neighbors algorithm, that describe commonalities between the data points per cluster.</p>
<figure>
<img src="https://upload.wikimedia.org/wikipedia/commons/d/d8/EM-Gaussian-data.svg" alt="Figure: Clustering example." /><figcaption aria-hidden="true">Figure: Clustering example.</figcaption>
</figure>
<p><a href="https://upload.wikimedia.org/wikipedia/commons/d/d8/EM-Gaussian-data.svg">Source: Wikipedia</a></p>
<p>Unsupervised techniques may allow insights into data at a stage in which its structure is yet unclear. It may often be helpful in a pre-processing step of data to select relevant features.</p>
<h3 id="supervised-learning">Supervised learning</h3>
<p>Algorithms which are supposed to infer or adapt a strategy for a designated task by inspection of meaningful training data. Examples:</p>
<ul>
<li>Spamfilters;</li>
<li>Recognition of objects in an digital image or on a video;</li>
<li>Voice recognition;</li>
<li>Load balancing in a network;</li>
<li>Semantic distances between phrases in natural language;</li>
<li>Sentiment analysis;</li>
<li>Imitation tasks, such as positioning photo realistic objects in a scenery;</li>
<li>Prediction of trends in time-series;</li>
<li>[…]</li>
</ul>
<p>As a fictitious example, consider the problem of mapping genome sequences to protein expressions. In an initial training step the supervised learning algorithm is presented known correlations between genome sequences and expressed proteins. After the completion of the training the algorithm may then be presented unseen genomes and asked to predict potentially expressed proteins.</p>
<figure>
<img src="01/supervised_learning.jpg" alt="Figure: Supervised Learning" /><figcaption aria-hidden="true">Figure: Supervised Learning</figcaption>
</figure>
<p>Instead of having to specify all rules comprising the algorithm manually, we would only need to specify the class of a task, while a in some to be specified sense optimal adaptation to a specific choice of task within the class is done automatically.</p>
<p>The task of assigning group memberships denoted by discreet class labels to data instances of input features is called <em>classification</em>. A typical multi-class classification task that we will look in more detail in this course is the recognition of hand written numbers.</p>
<p>The task of prediction of continuous outcomes is called <em>regression</em>. A typical example is to predict a continuous asset price response variable) by means of certain input features (predictors).</p>
<p>The performance of the learning algorithm carrying out the desired task naturally and often crucially depends on the quality of the mostly hand-annotated and/or manually cleaned training examples, which is often hard or costly to come by. This is one of the two major obstacles for industry-readiness of a lot of these algorithms – the other being the typically a missing level of transparency of how the algorithm came to its conclusions when completing a task.</p>
<p><strong>Reinforcement Learning:</strong> A very interesting approach of supervised learning that does not require initially annotated training data is is <em>reinforcement learning</em>. It requires an <em>agent</em>, i.e., machine learning program plus artifact, that has means to evaluate its interaction with its <em>environment</em> in which it operates in view of optimality of task to accomplish. Here, the feedback for the algorithms is not the ground truth label or value as for other supervised learners but a so-called <em>reward</em> that evaluates how well the conducted action was perceived in view of the given task. Actions are chosen on the basis of the current state, previous knowledge (exploitation), and trial-and-error (exploration). Every interaction is evaluated by the agent by means of the reward function and adds to the training data. The goal of reinforcement learning is to maximizes the overall accumulated reward from start to completion of the task.</p>
<figure>
<img src="01/reinforcement_learning.jpg" alt="Figure: Reinforcement Learning" /><figcaption aria-hidden="true">Figure: Reinforcement Learning</figcaption>
</figure>
<p>In some sense reinforcement learning offers an intelligent search, balancing exploitation and exploration, to find an optimal interaction policy for a certain task in a previously unknown environment. Its success usually relies on situations that although, theoretically, may involved a very large number of combinations of possible sequences of states and actions, knowledge of only a comparably small subset is already sufficient to carry out the tasks in some optimal regime. Examples:</p>
<ul>
<li>Movement of a robot in unknown terrain or under varying conditions, such as the Mars rover;</li>
<li>Self-driving car;</li>
<li>Beating the Go champions (nine rules and the pattern of the board allow to evaluate all possible interactions – however, the number of combinations is astronomical);</li>
<li>Getting high-scores in Atari games (the change of high-scores in relation to the change of pixel configurations on screen with respect to keyboard commands allow an evaluation of all possible interactions – also here, the number of combinations is very large);</li>
<li>[…]</li>
</ul>
<h4 id="dataism">Dataism</h4>
<p>Here, an early word of warning concerning the emergence of <em>dataism</em> in machine learning may be worthwhile. Since the age of big data, there is a tendency to believe that any problem can be solved simply by acquiring sufficiently large sets of data. Translated in our setting, given a problem, we would then only need enough training data to train a supervised learner that would in turn solve the problem eventually. As this may not even be false for some problems (at least in the academic limit <span class="math inline">\(\operatorname{time}\to\infty\)</span>), in general, it will typically turn out to be inefficient or unfeasible – and even more important, unnecessary.</p>
<p>Data contains historic information about empirical relative frequencies of events and their correlations that occurred under certain circumstances. A main novelty in machine learning compared to traditional statistics is the ability of some algorithms to efficiently pick up such correlations even in highly dimensional data where it might not be feasible anymore to compute a correlation matrix. Frequencies and their correlations, however, carry little meaning if there is not at least a first theory or rudimentary understanding what the events are and under which circumstances they were observed. Even if the meaning is settled, the inventor of Bayesian networks rightfully adds:</p>
<blockquote>
<p><em>It is critical to realize that data are profoundly dumb about causal relationships.</em></p>
<p>– Judea Pearl</p>
</blockquote>
<p>In other words, a mere correlation is symmetric in time. Does the rooster raise the sun by means of its morning crow since or is it the other way round that the rising sun encourages the rooster to crow? Both theories will lead to the same prediction of the high correlation between the roosters crow and the rising sun, which alone cannot discriminate between the two theories.</p>
<p>But even if it is only correlation we are after, the many supervised learning algorithms have inherent different biases in picking up particular correlations. Therefore, usually a lot of though is invested in choosing a particular model for a particular task. This is summarized in the famous <em>“free lunch”</em> theorems by the works of David Wolpert et al., which very loosely and vague could be summarized as <em>“a general-purpose universal optimization strategy is theoretically impossible, and the only way one strategy can outperform another is if it is specialized to the specific problem under consideration”.</em></p>
<p>Finally, the efficient search for correlations conducted by some machine learning algorithms often comes at the cost of a, in best case, well-performing trained supervised machine learning model that merely acts as a black-box. The information of the identified correlations used for a particular classification task are usually encoded in a non-trivial way in over many millions of model parameters. To extract a human-level justification for a particular classification from these model parameters is usually not feasible anymore. Hence, many interesting questions arise which have given momentum again to the field of <em>Explainable AI</em>. How can algorithms be adapted to deliver human-level justifications? How may self-driving cars be certified roadworthy?</p>
<p>Despite this warning about a too blind believe in data alone, it is a fact that supervised machine learning algorithms deliver astonishing results that in some cases outperform humans in terms of both classification efficiency and time.</p>
<h2 id="our-goals">Our goals</h2>
<p>Our main focus in this course will lie on algorithms of supervised learning. The typical situation we will examine is characterized as follows:</p>
<ul>
<li>The algorithm of interest is based on a optimization program.</li>
<li>Only a finite ensemble of training data is known,</li>
<li>while the distribution of all possible training data is unknown.</li>
</ul>
<p>This puts our study in the intersection between algorithm engineering, optimization theory, and statistics. Our goals are:</p>
<ul>
<li>Course <em>Mathematics and Applications of Machine Learning:</em>
<ul>
<li>Heuristics;</li>
<li>Model and algorithm definitions;</li>
<li>Mathematical discussion;</li>
<li>Mathematical foundations:
<ul>
<li>Optimization</li>
<li>Approximation</li>
<li>Statistics</li>
</ul></li>
</ul></li>
<li><em>Common tutorial sessions:</em>
<ul>
<li>Implement several learning algorithms in Python and evaluate their performance;</li>
<li>Mathematical exercises.</li>
</ul></li>
<li>Course <em>Mathematical Statistics and Applications of Machine Learning:</em>
<ul>
<li>Deeper dive into statistical learning theory;</li>
<li>Additional exercises regarding those topics.</li>
</ul></li>
</ul>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
