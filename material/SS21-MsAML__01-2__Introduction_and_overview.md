# MsAML-01-1 [[PDF](SS21-MsAML__01-2__Introduction_and_overview.pdf)]

1. [Introduction and overview](#introduction-and-overview)
2. [What is *artificial intelligence* (AI)?](#what-is-*artificial-intelligence*-(ai)?)
   * [Acting humanly: The Turing test approach](#acting-humanly:-the-turing-test-approach)
   * [Thinking humanly: The cognitive modeling approach](#thinking-humanly:-the-cognitive-modeling-approach)
   * [Thinking rationally: The “laws of thought” approach](#thinking-rationally:-the-“laws-of-thought”-approach)
   * [Acting rationally: The rational agent approach](#acting-rationally:-the-rational-agent-approach)
   * [Influences](#influences)
   * [A breakdown of historical periods](#a-breakdown-of-historical-periods)
3. [What is machine learning (ML)?](#what-is-machine-learning-(ml)?)
   * [Unsupervised learning](#unsupervised-learning)
   * [Supervised learning](#supervised-learning)
     * [Dataism](#dataism)
4. [Our goals](#our-goals)

Back to [index](index.md).


## Introduction and overview 

Before diving into the topic let us settle for a common vocabulary and what we
would like to achieve with our study. 

We start with a very broad overview and discussion of artificial intelligence
taken from
[Artificial Intelligence: A Modern Approach. Stuart J. Russell, Peter Norvig, Prentice Hall, 2010](https://www.pearson.com/store/p/artificial-intelligence-a-modern-approach/P100000667303/9780136042594)
and afterwards put machine learning and the aims of this lecture in
perspective.

## What is *artificial intelligence* (AI)?

* For many thousands of years, we have been trying to understand how we think;
* AI attempts to go a step further: 
    * in not to understand but also to build *intelligent*
      entities.
* AI is one of the newest fields in science and engineering (started shortly
  after World War II);
* Possible definitions:
    * Acting humanly:
        
        > *"The art of creating machines that perform functions that require
        > intelligence when performed by people."* 
        > 
        > -- Kurzweil, 1990

        > *"The study of how to make computers do things at which, at the
        > moment, people are better."*
        > 
        > -- Rich and Knight, 1991
        
    * Thinking humanly:

        > *"The exciting new effort to make computers think [...] machines
        > with minds, in the full and literal sense."*
        >     
        > -- Haugeland, 1985

        > *"[The automation of] activities that we associate with human
        > thinking, activities such as decision-making, problem solving,
        > learning [...]"*
        >     
        > -- Bellman, 1978 

    * Acting rationally:

        > *"Computational Intelligence is the study of the design of intelligent
        > agents."*
        >
        > -- Poole et al., 1998
          
        > *"AI [...] is concerned with intelligent behavior in artifacts."*
        >
        > -- Nilsson, 1998

    * Thinking rationally:

        > *"The study of mental faculties through the use of computational
        > models."*
        >
        > -- Charniak and McDermott, 1985

        > *"The study of the computations that make it possible to perceive,
        > reason, and act."*
        >
        > -- Winston, 1992


### Acting humanly: The Turing test approach

* In [1950 Turning](https://academic.oup.com/mind/article/LIX/236/433/986238) 
  devised a test to provide a satisfactory operational definition of
  intelligence;
* A computer passes the test if a human interrogator, after posing some
  written questions, cannot tell whether the written responses come from a
  person or from a computer;
* The computer needs to posses the following features:
    * **natural language processing:** to communicate in, e.g., English
    * **knowledge representation:** to store the information
    * **automated reasoning:** to use the stored information, to answer
      question, and to draw conclusions
    * **machine learning:** to adapt to new circumstances, and extrapolate
      and detect patterns
* The Turing test remains relevant even today but less from the
  engineering and more from the philosophical stance, i.e., in the following
  spirit: 
  * The quest for “artificial flight” succeeded when the Wright
  brothers and others stopped imitating birds and started using wind tunnels
  and learning about aerodynamics. 
  * Aeronautical engineering texts do not define the goal of their field as
  making "machines that fly so exactly like pigeons that they can fool even
  other pigeons."


### Thinking humanly: The cognitive modeling approach

* To tell whether a program "thinks like a human" we need to learn what
  humanely thinking is:
  * Observe our thoughts as they go by;
  * Observe persons during a task
  * Psychological / neuroscientific experiments
* For our endeavor, however, it will be good practice to keep the
  fields such as cognitive science, neuroscience, psychology and
  philosophy separated as long as it is possible.


### Thinking rationally: The “laws of thought” approach

Use of logical reasoning and argumentation. For an intentionally naive but
characterizing example consider:

* **deduction:** A general rule applied to a particular case
  implies a trivial result.
    
  | Input  | Implication | Example                              |
  | -      | -           | -                                    |
  | *RULE* |             | On a planet its sun rises every day. |
  | *CASE* |             | We are on a planet.                  |
  |        | *RESULT*    | The sun rose every day.              |

  The realm of mathematics. 

* **induction:** From a trivial result in a particular case we *hope*
  to infer the general rule.
                
  | Input    | Implication | Example                              |
  | -        | -           | -                                    |
  |          | *RULE*      | On a planet its sun rises every day. |
  | *CASE*   |             | We are on a planet.                  |
  | *RESULT* |             | The sun rose every day.              |

  Mostly the realm of natural sciences and there is no guarantee but room for
  interpretation and dispute.

* **abduction:** From a general rule and a trivial result we *hope*
  to infer the particular case.
                
  | Input    | Implication | Example                              |
  | -        | -           | -                                    |
  | *RULE*   |             | On a planet its sun rises every day. |
  |          | *CASE*      | We are on a planet.                  |
  | *RESULT* |             | The sun rose every day.              |

  Similar to induction and but rather seldom used.
        

### Acting rationally: The rational agent approach

* Create *agents* (e.g., computer programs) that operate autonomously,
  perceive their environment, persist over a prolonged time period,
  adapt to change, and create and pursue goals. 
* Making correct inferences (as in the "laws of thought" approach) is
  the extreme case of being a rational agent.
* In many situations, however, correct inferences are not possible, e.g.:
    * insufficient understanding of the environment;
    * not enough input data to base a decision on.
* A rational agent is one that acts so as to achieve the *best* outcome
  or, when there is uncertainty, the best expected outcome.

We will go down this road:

* The standard of rationality is mathematically well defined and
  completely general.
* We may exploit this, spell out specific designs, and check how they
  perform in certain environments. For instance:
    * The "probably approximately correct (PAC)" framework
* Human behavior, on the other hand, is well adapted for one specific
  environment and is defined by, well, the sum total of all the things
  that humans do.


### Influences

Though AI is rather young it is a very multi-disciplinary field:

* Philosophy:
    * Can formal rules be used to draw valid conclusions? 
    * How does the mind arise from a physical brain?
    * Where does knowledge come from?
    * How does knowledge lead to action?
* Mathematics:
    * What are the formal rules to draw valid conclusions?
    * What can be computed?
    * How do we reason with uncertain information?
* Neuroscience:
    * How do brains process information?
* Psychology
    * How do humans and animals think and act?
* Computer engineering
    * How can we build an efficient computer -- the artifact that we want to charge with *intelligence*?
* Control theory and cybernetics:
    * How can artifacts operate under their own control?
* Linguistics:
    * How does language relate to thought?
* Finance and economics:
    * How should we make decisions so as to maximize payoff?
    * How should we do this when others may not go along?
    * How should we do this when the payoff may be far in the future?
* [...]


### A breakdown of historical periods

* **1943–1955:**  The gestation of artificial intelligence
    * Model of artificial neurons by [Warren McCulloch and Walter Pitts in 1943](https://link.springer.com/article/10.1007/BF02478259); 
    * Turning gave lectures on AI as soon as 1947.
* **1956:**  The birth of artificial intelligence
    * John McCarthy convinced Marvin Minsky, Claude Shannon, and Nathaniel
      Rochester to help him bring together U.S. researchers interested in
      automata theory, neural nets, and the study of intelligence at
      a two-month workshop in Dartmouth.
* **1952–1969:**  Early enthusiasm, great expectations
    * First problem solvers, game players, theorem provers;
    * John McCarthy referred to this period as the “Look, Ma, no hands!”
      era;
    * Creation of LISP; 
    * Perceptron by [Frank Rosenblatt in 1958](https://psycnet.apa.org/record/1959-09865-001);
    * Adalines (adaptive linear neuron) by Bernie Widrow and Marcian Hoff
      in 1960 :cite:`widrow_adapting_1960`;
* **1966–1973:** A dose of reality
    * Try and error -- combinatorial explosion;
    * Lack of computational resources.
* **1969–1979:** Knowledge-based systems: The key to power?
    * Algorithms using of domain-specific knowledge instead of general-purpose solvers;
    * Expert systems for medical diagnosis;
    * Incorporation of uncertainty.
* **1980–present:** AI becomes an industry
    * Optimization of logistics;
    * Sudden boom but only few projects lived up to their expectations;
    * AI winter.
* **1986–present:** The return of neural networks
    * The *back-propagation* algorithm for training neural networks was reinvented in :cite:`rumelhart_learning_1986`;
* **1987–present:** AI adopts the scientific method 
    * Hidden Markov models;
    * Bayesian networks;
* **1995–present:** 
    * The Internet pushes the development of intelligent (?) agents, e.g.:
        * chatbots
        * recommender systems
        * aggregates
    * Access to computation resources at sufficient speed.
    * The *big data* age: Huge amount of labeled training data available, e.g.:
        * Dictionaries
        * Word corpora on different topics
        * Wordnets
        * Wikipedia
        * Google
    * Founders of AI disappointed with current state:
        * AI should return to its roots of striving for, in Herbert Simon’s
          words, "machines that think, that learn and that create."
    * Now long state-of-the-art -- some examples:
        * Spam fighting: Most adaptation done my machine learning algorithms
        * Speech recognition: Siri, 
        * Face recognition: Facebook, Apple Photos, Google Photos
        * Game playing: IBM's deep blue chess player against world champion Garry Kasparov
        * Autonomous planning and scheduling: NASA's mars rover 
        * Robotic vehicles: Tesla's self-driven car
        * Machine Translation: Google Translate
    * And in addition some astonishing academic advances 
    that, even though, industry readiness may be still open:

        * 2011 IBM Watson triumphs at *Jeopardy!*
        * 2012 Google's X Lab object recognition of cats in YouTube videos
        * 2014 DeepMind's AI playing Atari Games
        * 2014 Generative Adversial Networks that generate or adapt content,
        such as, photorealistic photos, videos, 3D models
        * 2015 DeepMind's AlphaGo beats Go champion
        * [...]


## What is machine learning (ML)?

Having a first overview, we come to the direction of our study, i.e., machine
learning. With this we, as well, might disappoint the founders -- in view of
Herbert Simon's quote above. However, in our defense we may claim that in
whichever direction AI might develop, machine learning will at least be an
extremely important stepping stone if not even stay an integral part in the
field:

* ML is a subfield of AI:

  > *"[Machine learning] gives computers the ability to learn without
  > being explicitly programmed"* 
  >
  > -- Samuel, 1959

  Put differently, one seeks algorithms which have general strategies at their
  expense or are able to adapt their strategies to solve a to some extend
  previously unknown task. 

* ML will help us dealing with large amounts of data:

    * data compression;
    * correlation detection;
    * pattern recognition;
    * structuring data;
    * classification of data;
    * extrapolation / prediction;
    * model fitting;
    * data driven decision support;
    * [...]


### Unsupervised learning

Structuring or filtering of data on the basis of predefined strategy and
ontologies. Examples:

* Data compression and dimensionality reduction;
* Assisted feature selection, correlation detection;
* Identification of data clusters, such as, galaxy clusters in astronomic
  observational data, scattering events in a particle collider, etc.;
* Process mining;
* Language modeling, e.g., with wordnets;
* Identification of customer groups based on the Facebook likes;
* [...]

As a fictitious example, consider the following plot of 2d data points with the
colors indicating a relation between the data points. From these relations the
shaded regions may be inferred by an unsupervised machine learning algorithm,
e.g., by a nearest-neighbors algorithm, that describe commonalities between the
data points per cluster.
   
![Figure: Clustering
example.](https://upload.wikimedia.org/wikipedia/commons/d/d8/EM-Gaussian-data.svg)

[Source: Wikipedia](https://upload.wikimedia.org/wikipedia/commons/d/d8/EM-Gaussian-data.svg)

Unsupervised techniques may allow insights into data at a stage in which its
structure is yet unclear. It may often be helpful in a pre-processing step of
data to select relevant features.
                    

### Supervised learning

Algorithms which are supposed to infer or adapt a strategy for a designated
task by inspection of meaningful training data. Examples:

* Spamfilters;
* Recognition of objects in an digital image or on a video;
* Voice recognition;
* Load balancing in a network;
* Semantic distances between phrases in natural language;
* Sentiment analysis;
* Imitation tasks, such as positioning photo realistic objects in a scenery;
* Prediction of trends in time-series;
* [...]

As a fictitious example, consider the problem of mapping genome sequences to
protein expressions. In an initial training step the supervised learning
algorithm is presented known correlations between genome sequences and
expressed proteins. After the completion of the training the algorithm may then
be presented unseen genomes and asked to predict potentially expressed
proteins.

![Figure: Supervised Learning](01/supervised_learning.jpg)

Instead of having to specify all rules comprising the algorithm manually, we
would only need to specify the class of a task, while a in some to be specified
sense optimal adaptation to a specific choice of task within the class is done
automatically.

The task of assigning group memberships denoted by discreet class labels to
data instances of input features is called *classification*. A typical
multi-class classification task that we will look in more detail in this course
is the recognition of hand written numbers.

The task of prediction of continuous outcomes is called *regression*. A typical
example is to predict a continuous asset price response variable) by means of
certain input features (predictors).

The performance of the learning algorithm carrying out the desired task
naturally and often crucially depends on the quality of the mostly
hand-annotated and/or manually cleaned training examples, which is often hard
or costly to come by. This is one of the two major obstacles for
industry-readiness of a lot of these algorithms -- the other being the
typically a missing level of transparency of how the algorithm came to its
conclusions when completing a task.

**Reinforcement Learning:** A very interesting approach of supervised learning
that does not require initially annotated training data is is *reinforcement
learning*. It requires an *agent*, i.e., machine learning program plus
artifact, that has means to evaluate its interaction with its *environment* in
which it operates in view of optimality of task to accomplish.  Here, the
feedback for the algorithms is not the ground truth label or value as for other
supervised learners but a so-called *reward* that evaluates how well the
conducted action was perceived in view of the given task.  Actions are chosen
on the basis of the current state, previous knowledge (exploitation), and
trial-and-error (exploration).  Every interaction is evaluated by the agent by
means of the reward function and adds to the training data. The goal of
reinforcement learning is to maximizes the overall accumulated reward from
start to completion of the task.

![Figure: Reinforcement Learning](01/reinforcement_learning.jpg)

In some sense reinforcement learning offers an intelligent search, balancing
exploitation and exploration, to find an optimal interaction policy for a
certain task in a previously unknown environment. Its success usually relies on
situations that although, theoretically, may involved a very large number of
combinations of possible sequences of states and actions, knowledge of
only a comparably small subset is already sufficient to carry out the tasks in
some optimal regime. Examples:

* Movement of a robot in unknown terrain or under varying conditions, such as
the Mars rover;
* Self-driving car;
* Beating the Go champions (nine rules and the pattern of the board
allow to evaluate all possible interactions --  however, the number of combinations is
astronomical);
* Getting high-scores in Atari games (the change of high-scores in relation to
the change of pixel configurations on screen with respect to keyboard commands
allow an evaluation of all possible interactions -- also here, the number of
combinations is very large);
* [...]


#### Dataism

Here, an early word of warning concerning the emergence of *dataism* in machine
learning may be worthwhile. Since the age of big data, there is a tendency to
believe that any problem can be solved simply by acquiring sufficiently large
sets of data. Translated in our setting, given a problem, we would then only
need enough training data to train a supervised learner that would in turn
solve the problem eventually. As this may not even be false for some problems (at
least in the academic limit $\operatorname{time}\to\infty$), in general, it
will typically turn out to be inefficient or unfeasible -- and even more
important, unnecessary.

Data contains historic information about empirical relative frequencies of
events and their correlations that occurred under certain circumstances. A main
novelty in machine learning compared to traditional statistics is the ability
of some algorithms to efficiently pick up such correlations even in highly
dimensional data where it might not be feasible anymore to compute a
correlation matrix. Frequencies and their correlations, however, carry little
meaning if there is not at least a first theory or rudimentary understanding
what the events are and under which circumstances they were observed.
Even if the meaning is settled, the inventor of Bayesian networks rightfully
adds:

> *It is critical to realize that data are profoundly dumb about causal
>   relationships.*
>
> -- Judea Pearl

In other words, a mere correlation is symmetric in time. Does the rooster raise
the sun by means of its morning crow since or is it the other way round that
the rising sun encourages the rooster to crow? Both theories will lead to the
same prediction of the high correlation between the roosters crow and the
rising sun, which alone cannot discriminate between the two theories. 

But even if it is only correlation we are after, the many supervised learning
algorithms have inherent different biases in picking up particular
correlations. Therefore, usually a lot of though is invested in choosing a
particular model for a particular task. This is summarized in the famous *"free
lunch"* theorems by the works of David Wolpert et al., which very loosely and
vague could be summarized as *"a general-purpose universal optimization
strategy is theoretically impossible, and the only way one strategy can
outperform another is if it is specialized to the specific problem under
consideration".* 

Finally, the efficient search for correlations conducted by some machine
learning algorithms often comes at the cost of a, in best case, well-performing
trained supervised machine learning model that merely acts as a black-box. The
information of the identified correlations used for a particular classification
task are usually encoded in a non-trivial way in over many millions of model
parameters. To extract a human-level justification for a particular
classification from these model parameters is usually not feasible anymore.
Hence, many interesting questions arise which have given momentum again to the
field of *Explainable AI*. How can algorithms be adapted to deliver human-level
justifications? How may self-driving cars be certified roadworthy? 

Despite this warning about a too blind believe in data alone, it is a fact that
supervised machine learning algorithms deliver astonishing results that in some
cases outperform humans in terms of both classification efficiency and time.


## Our goals

Our main focus in this course will lie on algorithms of supervised learning.
The typical situation we will examine is characterized as follows:

* The algorithm of interest is based on a optimization program.
* Only a finite ensemble of training data is known,
* while the distribution of all possible training data is unknown.

This puts our study in the intersection between algorithm engineering,
optimization theory, and statistics. Our goals are:

* Course *Mathematics and Applications of Machine Learning:*
  * Heuristics;
  * Model and algorithm definitions;
  * Mathematical discussion;
  * Mathematical foundations:
    * Optimization
    * Approximation
    * Statistics
* *Common tutorial sessions:*
  * Implement several learning algorithms in Python and evaluate their performance;
  * Mathematical exercises.
* Course *Mathematical Statistics and Applications of Machine Learning:*
  * Deeper dive into statistical learning theory;
  * Additional exercises regarding those topics.
