<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
<head>
  <meta charset="utf-8" />
  <meta name="generator" content="pandoc" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
  <meta name="author" content="Dirk - André Deckert" />
  <title>SS21-MsAML__01-S1__Statistical_framework</title>
  <style>
    code{white-space: pre-wrap;}
    span.smallcaps{font-variant: small-caps;}
    span.underline{text-decoration: underline;}
    div.column{display: inline-block; vertical-align: top; width: 50%;}
    div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
    ul.task-list{list-style: none;}
  </style>
  <link rel="stylesheet" href="./_template/styles.css" />
  <script src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml-full.js" type="text/javascript"></script>
  <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
  <![endif]-->
</head>
<body>
<h1 id="msaml-01-s1-pdf">MsAML 01-S1 [<a href="SS21-MsAML__01-S1__Statistical_framework.pdf">PDF</a>]</h1>
<ol type="1">
<li><a href="#statistical-framework">Statistical framework</a>
<ul>
<li><a href="#classification">Classification</a></li>
<li><a href="#regression">Regression</a></li>
<li><a href="#a-challenge-in-statistics">A challenge in statistics</a></li>
</ul></li>
</ol>
<p>Back to <a href="index.html">index</a>.</p>
<h2 id="statistical-framework">Statistical framework</h2>
<p>Base on the setting introduced in the module <a href="./SS21-MsAML__01-3__Supervised_learning_setting.html">Supervised learning setting</a> a supervised learning algorithm <span class="math inline">\(\mathcal A\)</span>, or short supervised learner, can be understood as a map of sequences of training data of variable length <span class="math display">\[\begin{align}
  s=(x^{(i)},y^{(i)})_{i=1,\ldots, N} 
\end{align}\]</span> to a hypothesis <span class="math display">\[\begin{align}
  h_s:\mathcal X\to\mathcal Y,
\end{align}\]</span> which then produces a label prediction <span class="math inline">\(h_s(x)\)</span> in label space <span class="math inline">\(\mathcal Y\)</span> for every features <span class="math inline">\(x\)</span> in feature space <span class="math inline">\(\mathcal X\)</span> in a way such that</p>
<ol type="1">
<li>it performs well on the known training data <span class="math inline">\(s\)</span>, i.e., has only few errors <span class="math inline">\(h_s(x^{(i)})\neq y^{(i)}\)</span>;</li>
<li>and has a good chance to generalize well to unseen data samples, i.e., to also accurately predict the actual labels <span class="math inline">\(y\in\mathcal Y\)</span> for previously unseen data <span class="math inline">\(x\in\mathcal X\)</span> with the <span class="math inline">\(h_s(x)\)</span>.</li>
</ol>
<p>In the following we want to introduce a mathematical framework to capture this rather vaguely formulates goal of learning more precisely.</p>
<p>In particular due to the above point 2. a probabilistic setting is natural:</p>
<ul>
<li><p>Potential features and label combinations <span class="math display">\[\begin{align}
  (x,y) \in \mathcal X\times\mathcal Y
\end{align}\]</span> shall be modeled by random variables <span class="math inline">\(X,Y\)</span>. Throughout the script we will try to use capital letters for random variables and lowercase letter for there values.</p></li>
<li><p>For simplicity we assume:</p>
<ul>
<li>the <span class="math inline">\(\sigma\)</span>-algebra over <span class="math inline">\(\mathcal X\times \mathcal Y\)</span> to be of product form;</li>
<li>P to be a measure on the <span class="math inline">\(\sigma\)</span>-algebra over <span class="math inline">\(\mathcal X\times\mathcal Y\)</span>;</li>
<li>and if not noted otherwise, all non-explicit functions used are assumed to be measurable.</li>
</ul></li>
<li><p>Training data is modeled by an i.i.d. family of random variables denoted by <span class="math display">\[\begin{align}
  S=(X^{(i)},Y^{(i)})_{1,\ldots,N}.
\end{align}\]</span></p></li>
<li><p>As further notation, let us introduce:</p>
<ul>
<li><span class="math inline">\(E\)</span> as expectation value w.r.t. <span class="math inline">\(P\)</span>.</li>
<li><span class="math inline">\(P^N\)</span> as the product measure on <span class="math inline">\((\mathcal X\times\mathcal Y)^N\)</span>.</li>
<li>For a distribution <span class="math inline">\(D\)</span> we will write <span class="math display">\[\begin{align}
  E_{Z\sim D}(f(Z))
\end{align}\]</span> to express that we take the expectation value of a function of random variable <span class="math inline">\(Z\)</span> which has distribution <span class="math inline">\(D\)</span>.</li>
<li>For <span class="math inline">\((Z_1,\ldots,Z_n)\)</span> i.i.d. according to distribution <span class="math inline">\(D\)</span> we write <span class="math display">\[\begin{align}
  (Z_1,\ldots,Z_n)\sim D^N.
\end{align}\]</span></li>
<li>For random variables <span class="math inline">\(F,G\)</span> we often write the conditional expectation as <span class="math display">\[\begin{align}
  E_F f(F,G) = E\left( f(F,G) \big| G \right).
\end{align}\]</span></li>
</ul></li>
</ul>
<dl>
<dt>👀</dt>
<dd>Recall the definition of conditional probability and expectation.
</dd>
</dl>
<p>Returning to our algorithm <span class="math inline">\(\mathcal A\)</span> in this mathematical setting, we need means to evaluate the accuracy of a hypothesis <span class="math inline">\(h\)</span> predicting the labels <span class="math display">\[\begin{align}
  h(x)\in\mathcal Y
\end{align}\]</span> for given features <span class="math display">\[\begin{align}
  x\in\mathcal X
\end{align}\]</span> and actual values <span class="math display">\[\begin{align}
  y\in\mathcal Y.
\end{align}\]</span> For this purpose, we introduce the function <span class="math display">\[\begin{align}
  L:\mathcal Y\times\mathcal Y\to \mathbb R
\end{align}\]</span> which we will evaluated the correctness of the predicted label <span class="math inline">\(h(x)\)</span> with respect tot the expected actual label <span class="math inline">\(y\)</span> by means of its value <span class="math display">\[\begin{align}
  L(y, h(x)).
\end{align}\]</span> This function has many names, e.g., <em>loss, cost, objective, … function</em>. Its purpose is to somewhat evaluate the accuracy of a prediction. Before we give concrete examples, think of it as being large if the prediction <span class="math inline">\(h(x)\)</span> has little in common with the actual label <span class="math inline">\(y\)</span> and small if it is actually a pretty close or equal.</p>
<p>Equipped with this probabilistic framework on this loss function, we can revisit the goal of learning:</p>
<dl>
<dt>Learning goal:</dt>
<dd>To find a hypothesis <span class="math inline">\(h\)</span> that, for a given loss function <span class="math inline">\(L\)</span>, minimizes the so-called risk <span class="math display">\[\begin{align}
R_h := E L(Y, h(X)).
  \end{align}\]</span> Notice that <span class="math inline">\(X,Y\)</span> are now random variables and <span class="math inline">\(E\)</span> is the expectation with respect to there product distribution. This expression of the risk also goes under the name <em>generalization error</em>.
</dd>
</dl>
<p>This seems to be a sensible and simple definition but it looks more innocent that maybe meets the eye at first sight. In particular, we will constantly be face with the following difficulty:</p>
<dl>
<dt>⚠️</dt>
<dd><span class="math inline">\(P\)</span>, i.e., the distribution of <span class="math inline">\(X,Y\)</span>, is typically unknown, which means that <span class="math inline">\(R_h\)</span> cannot even be computed.
</dd>
</dl>
<p>All that is known to the supervised learning is the training data sample tuple <span class="math inline">\(s=(x^{(i)},y^{(i)})_{i=1,\ldots,N}\)</span>. Hence, we will have to make good use of statistics in order to nevertheless derive upper bounds on the risk or generalization error, usually called <em>generalization bounds</em>, although we known almost nothing about the distribution of <span class="math inline">\(X,Y\)</span>. For this reason the field of supervises machine learning we will be looking at is also called <em>statistical learning theory</em>.</p>
<dl>
<dt>👀</dt>
<dd>Please keep this point in mind until we have our first example of a generalization bound and revisit this thought to appreciate how it was even possible to derive it with so little information.
</dd>
</dl>
<p>Before we dive into the generalization bounds, let us introduce some further vocabulary. One distinguishes between two general types of machine learning problems:</p>
<h3 id="classification">Classification</h3>
<p>If the set of labels <span class="math inline">\(\mathcal Y\)</span> is discreet, one refers to the learning problem as <em>classification</em>. The hypothesis <span class="math inline">\(h\)</span> is called a <em>classifier</em> and the elements of <span class="math inline">\(\mathcal Y\)</span> are called <em>class labels</em>.</p>
<p>A typical loss function is given by <span class="math display">\[\begin{align}
  L(y,y&#39;) = 1_{y\neq y&#39;},
\end{align}\]</span> where <span class="math display">\[\begin{align}
  1_{y\neq y&#39;}
  =
  \begin{cases}
    1 \text{ for }{y\neq y&#39;}\\
    0 \text{ otherwise}
  \end{cases}.
\end{align}\]</span></p>
<p>The risk then takes the form <span class="math display">\[\begin{align}
  R(h)
  &amp;=
  EL(Y,h(X))\\
  &amp;=
  E 1_{Y\neq h(X)}\\
  &amp;=
  P(Y\neq h(X)),
\end{align}\]</span> which is called <em>error probability</em>.</p>
<p>In case, of <span class="math inline">\(|\mathcal Y|=2\)</span>, e.g., for class labels <span class="math inline">\(\mathcal Y=\{-1,1\}\)</span> or <span class="math inline">\(\mathcal Y=\{0,1\}\)</span>, one calls the classification problem <em>binary classification</em>.</p>
<p>Note that the framework we are introducing does not necessarily require that there is only one actual label <span class="math inline">\(y\in\mathcal Y\)</span> for each feature sample <span class="math inline">\(x\in\mathcal X\)</span>, in other words that there is a map <span class="math display">\[\begin{align}
  c:\mathcal X\to\mathcal Y
\end{align}\]</span> such that <span class="math display">\[\begin{align}
  P(Y=y|X=x)
  &amp;=
  \frac{P(X=x \wedge Y=y}{P(X=x)}
  \\
  &amp;=
  1_{y=c(x)}.
\end{align}\]</span> If this is the case, <span class="math inline">\(c\)</span> is usually called a <em>concept</em> and the problem is referred to as <em>deterministic</em>. For example, in case of noisy data containing classification errors, this might not be true anymore, but also for general classification problems, the existence of a concept <span class="math inline">\(c\)</span> may not be justified. Consider, for example, the classification of the gender by means of height for men and women. Certainly, at least in the mid ranges any large enough training data set will show some samples which equal features but different labels.</p>
<p>In general, the lower bound of the error probability is ruled by the <em>Bayes classifier</em>: <span class="math display">\[\begin{align}
  h_B(x)
  &amp;:=
  \text{sign}\left(E(Y|X=x)\right) \\
  &amp;=
  \sum_{y\in\mathcal Y} y P(Y=y|X=x),
\end{align}\]</span> where <span class="math display">\[\begin{align}
  \text{sign}
  =
  \begin{cases}
    +1 \text{ for }x\geq 0\\
    -1 \text{otherwise}
  \end{cases}
\end{align}\]</span></p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="bayes-classifier">(Bayes classifier)</h5>
For <span class="math inline">\(\mathcal Y=\{-1,+1\}\)</span> and <span class="math inline">\(L(y,y&#39;)=1_{y\neq y&#39;}\)</span> we have: <span class="math display">\[\begin{align}
\forall h\in\mathcal Y^{\mathcal X}: R(h_B)\leq R(h).
  \end{align}\]</span>
</dd>
</dl>
<p><strong>Proof:</strong> ✍ Homework.</p>
<p>The theorem entails that the Bayes classifier minimizes the risk. To measure the performance of other classifiers <span class="math inline">\(h\)</span> it is therefore useful to consider the difference <span class="math display">\[\begin{align}
  R(h) - R(h_B),
\end{align}\]</span> which is called the <em>excess risk</em>.</p>
<h3 id="regression">Regression</h3>
<p>In the case of continuous <span class="math inline">\(\mathcal Y\)</span>, one calls the learning problem <em>regression</em>. The feature sample <span class="math inline">\(x\in\mathcal X\)</span> is referred to as <em>predictor</em> and the value <span class="math inline">\(y\in\mathcal Y\)</span> as <em>response</em>.</p>
<dl>
<dt>👀</dt>
<dd>The term regression was originally coined by Galton as “regression towards mediocrity” to describe the observation that ensembles of children of comparably small and tall parents tend to have taller and smaller children, respectively.
</dd>
</dl>
<p>A typical loss function is of the form <span class="math display">\[\begin{align}
  L(y,y&#39;) = |y-y&#39;|^2,
\end{align}\]</span> for which the corresponding risk <span class="math display">\[\begin{align}
  R(h)=EL(Y,h(x))
\end{align}\]</span> is called <em>mean square error</em>.</p>
<dl>
<dt>👀</dt>
<dd>A familiar regression example is least squares polynomial fitting.
</dd>
</dl>
<p>One of the reasons for its popularity is its geometric nature:</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="geometric-nature-of-conditional-expectation">(Geometric nature of conditional expectation)</h5>
Suppose <span class="math inline">\(EX^2, EY^2&lt;\infty\)</span>, then: <span class="math display">\[\begin{align}
R(h) 
= 
\underbrace{E\big|Y-E(Y|X)\big|^2}_{\text{vanishes in the deterministic case}}
+
\underbrace{E\big|h(X)-E(Y|X)\big|^2}_{\text{$L^2$-distance between hypothesis and $E(Y|X)$}}
  \end{align}\]</span>
</dd>
</dl>
<p><strong>Proof:</strong> ✍ Homework.</p>
<p>The function <span class="math inline">\(r(x):=E(Y|X=x)\)</span> is called <em>regression function</em> which reflect a best guest of the label despite the noise – recall the discussion in the discreet setting above.</p>
<h3 id="a-challenge-in-statistics">A challenge in statistics</h3>
<p>Coming back to the question of how a learning algorithm <span class="math inline">\(\mathcal A\)</span> can attempt to minimize the risk <span class="math inline">\(R(h)\)</span> over accessible hypothesis functions <span class="math inline">\(h\)</span>, we recall that the sole input information is given by the training data sequence: <span class="math display">\[\begin{align}
  s=(x^{(i)},y^{(i)})_{1,\ldots,N}.
\end{align}\]</span></p>
<p>As we have already observed the risk <span class="math inline">\(R(h)\)</span> itself cannot be computed, the only accessible quantity is the <em>empirical risk</em>: <span class="math display">\[\begin{align}
  \widehat R(h) 
  :=
  \frac{1}{N}\sum_{i=1}^N L\big(y^{(i)},h(x^{(i)})\big)
\end{align}\]</span> In the case of <span class="math inline">\(L(y,y&#39;)=1_{y\neq y&#39;}\)</span> one case this quantity <em>empirical error</em>.</p>
<p>An algorithm could now attempt to minimize <span class="math inline">\(\widehat R(h)\)</span> over accessible hypothesis function <span class="math inline">\(h\)</span>. Suppose a minimizer is given by <span class="math inline">\(\widehat h\)</span>, which then inherently depends on the training data <span class="math inline">\(s\)</span>. One may hope that, for sufficiently large <span class="math inline">\(N\)</span>, the quantities <span class="math inline">\(\widehat R(\widehat h)\)</span> and <span class="math inline">\(R(\widehat h)\)</span> are closely related.</p>
<p>To substantiate this hope will be the main objective of this statistics part of the course.</p>
<p>Of course, a straight-forward relation is:</p>
<dl>
<dt>Theorem</dt>
<dd><h5 id="expectation-of-the-empirical-risk">(Expectation of the empirical risk)</h5>
For any hypothesis <span class="math inline">\(h\)</span> we have <span class="math inline">\(E_{S\sim P^N}\widehat R(h)=R(h)\)</span>.
</dd>
</dl>
<p><strong>Proof:</strong> ✍ Homework.</p>
<p>However, this is academic and we would like to understand, how the difference between <span class="math inline">\(\widehat R(h)\)</span> and <span class="math inline">\(R(h)\)</span> behave depending on <span class="math inline">\(N\)</span>.</p>
<p>Note also that the minimization problem to find <span class="math inline">\(\widehat h\)</span> may be quite subtle. Although for finite <span class="math inline">\(|\mathcal Y|\)</span> there is always a minimizer <span class="math inline">\(\widehat h\in\text{range}\mathcal A\)</span> such that <span class="math display">\[\begin{align}
  \inf_{h\in\text{range}\mathcal A} \widehat R(h)= \widehat R(\widehat h),
\end{align}\]</span> since the problem reduces to a search for a discreet function <span class="math display">\[\begin{align}
  h:\{x^{(1)},\ldots, x^{(N)}\} \to \mathcal Y
\end{align}\]</span> in the range of <span class="math inline">\(\mathcal A\)</span> of which there are only finite many, finding a minimum may yet be a very computationally heavy task.</p>
<p>As we will see along the way, there is often only one ingredient, that will make this taks feasible: <em>Prior knowledge</em>. The latter can often be encoded by means of convenient restrictions on the range of <span class="math inline">\(\mathcal A\)</span> and convenient choices of loss function.</p>
<p>This ends our introduction to the statistical framework. The goal of the next module will be to understand better the nature of the difference between <span class="math inline">\(\widehat R(\widehat h)\)</span> and the actual quantity <span class="math inline">\(R(\widehat h)\)</span> we are interested in.</p>
<p>➢ Next session!</p>
<div class="license">
    <!-- <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"> -->
    <!--     <img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /> -->
    <!-- </a> -->
    By <a href="https://www.mathematik.uni-muenchen.de/~deckert/">D.-A. Deckert</a> licensed under a 
    <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
</div>
</body>
</html>
